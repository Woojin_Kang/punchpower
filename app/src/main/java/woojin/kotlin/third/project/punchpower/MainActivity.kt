package woojin.kotlin.third.project.punchpower

import android.animation.*
import android.content.Context
import android.content.Intent
import android.graphics.drawable.AnimatedImageDrawable
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.IntegerRes
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.util.*

class MainActivity : AppCompatActivity() {

    var maxPower = 0.0
    var isStart = false
    var startTime = 0L

    val sensorManager: SensorManager by lazy {
        getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    val eventListener: SensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

        }

        override fun onSensorChanged(event: SensorEvent?) {
            event?.let {
                if (event.sensor.type != Sensor.TYPE_LINEAR_ACCELERATION) return@let

                val power =
                    Math.pow(event.values[0].toDouble(), 2.0) + Math.pow(
                        event.values[1].toDouble(),
                        2.0
                    ) + Math.pow(event.values[2].toDouble(), 2.0)

                //측정된 펀치력이 20이 넘고 , 아직 측정이 시작되지 않은 경우
                if (power > 20 && !isStart) {
                    startTime = System.currentTimeMillis()
                    isStart = true
                }

                if (isStart) {

                    imageView.clearAnimation()

                    if (maxPower < power) maxPower = power

                    stateLabel.text = "펀치력을 측정하고 있습니다."

                    if (System.currentTimeMillis() - startTime > 3000) {
                        isStart = false
                        punchPowerTestComplete(maxPower)
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        initGame()
    }

    fun initGame() {
        maxPower = 0.0
        isStart = false
        startTime = 0L
        stateLabel.text = "핸드폰을 손에쥐고 주먹을 내지르세요"

        sensorManager.registerListener(
            eventListener,
            sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
            SensorManager.SENSOR_DELAY_NORMAL
        )

//        imageView.startAnimation(AnimationUtils.loadAnimation(this@MainActivity, R.anim.tran))
//        imageView.startAnimation(AnimationUtils.loadAnimation(this@MainActivity, R.anim.rotate))
//        imageView.startAnimation(AnimationUtils.loadAnimation(this@MainActivity, R.anim.alpha_scale))

//        val animation = AnimationUtils.loadAnimation(this@MainActivity, R.anim.alpha_scale)
//        imageView.startAnimation(animation)
//
//        animation.setAnimationListener(object : Animation.AnimationListener {
//            override fun onAnimationRepeat(p0: Animation?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onAnimationEnd(p0: Animation?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onAnimationStart(p0: Animation?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//        })

        /**
         * 속성 애니매이션
         */
//        AnimatorInflater.loadAnimator(this@MainActivity, R.animator.property_animation).apply {
//            addListener(object: AnimatorListenerAdapter(){
//                override fun onAnimationEnd(animation: Animator?) {
//                    start()
//                }
//            })
//
//            setTarget(imageView)
//            start()
//        }

        /**
         * 속성 애니매이션(배경색)
         */

        AnimatorInflater.loadAnimator(this@MainActivity, R.animator.color_anim).apply {
            val colorAnimator = this@apply as? ObjectAnimator

            colorAnimator?.apply {
                setEvaluator(ArgbEvaluator())
                target = window.decorView.findViewById(android.R.id.content)
                start()
            }
        }
    }

    fun punchPowerTestComplete(power: Double) {
        Log.d("MainActivity", "측정완료: power:${String.format("%.5f", power)}")
        sensorManager.unregisterListener(eventListener)
        val intent = Intent(this@MainActivity, ResultActivity::class.java)
        intent.putExtra("power", power)
        startActivity(intent)
    }

    override fun onStop() {
        super.onStop()
        try {
            sensorManager.unregisterListener(eventListener)
        } catch (e: Exception) {

        }
    }
}
